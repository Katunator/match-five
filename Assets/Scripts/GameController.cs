﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private const int M_MAP_SIZE = 9;
    private const int M_NEXT_BALL_COUNT = 3;
    private const int M_MATCH = 5;

    [SerializeField]
    private GameObject[] m_ballPrefabs;
    [SerializeField]
    private GameObject m_pathBallPrefab;
    [SerializeField]
    private GameObject m_mapPrefab;
    [SerializeField]
    private CanvasController m_canvasController;
    [SerializeField]
    private int m_cost = 2;

    private float m_timer;

    private bool m_ballIsComingSoon;

    private Vector3 m_mousePos;

    // map [x(right -> left), y(bottom -> top)]
    private Ball[,] m_map = new Ball[M_MAP_SIZE, M_MAP_SIZE];

    private Ball[] m_nextBalls = new Ball[M_NEXT_BALL_COUNT];

    private Ball m_pickedUpBall;

    private List<SPlace> m_freePlaces = new List<SPlace>();
    private List<Ball> m_destroyLines = new List<Ball>();
    private Stack<SPlace> m_ballPath = new Stack<SPlace>();

    private bool m_MouseOnTheMap
    {
        get
        {
            float x = m_mousePos.x;
            float y = m_mousePos.y;
            return (-0.5f < x) && (x < M_MAP_SIZE - 1 + 0.5f) && (-0.5f < y) && (y < M_MAP_SIZE - 1 + 0.5f);
        }
    }

    private SPlace m_MousePlace
    {
        get
        {
            int x = Mathf.RoundToInt(Mathf.Clamp(m_mousePos.x, 0f, M_MAP_SIZE - 1f));
            int y = Mathf.RoundToInt(Mathf.Clamp(m_mousePos.y, 0f, M_MAP_SIZE - 1f));

            return new SPlace(x, y);
        }
    }

    private void Start()
    {
        for (int i = 0; i < M_NEXT_BALL_COUNT; i++)
        {
            m_nextBalls[i] = new Ball(0, m_ballPrefabs[0], new SPlace(M_MAP_SIZE + 1 + i, M_MAP_SIZE - 1));
        }
    }

    private bool IsFree(SPlace place)
    {
        return m_freePlaces.Contains(place);
    }

    private void TryPickUpBall()
    {
        if (!IsFree(m_MousePlace))
        {
            m_pickedUpBall = new Ball(m_map[m_MousePlace.X, m_MousePlace.Y]);

            m_pickedUpBall.MoveUp();
            m_pickedUpBall.MoveTo(m_MousePlace);
            m_pickedUpBall.Half = true;

            m_map[m_MousePlace.X, m_MousePlace.Y].Half = true;
        }
    }

    private void TryDragBall()
    {
        // don't let go out of the map
        float x = Mathf.Clamp(m_mousePos.x, 0f, M_MAP_SIZE - 1f);
        float y = Mathf.Clamp(m_mousePos.y, 0f, M_MAP_SIZE - 1f);

        m_pickedUpBall.MoveTo(x, y);
    }

    private void FillInMap(Ball ball, SPlace place)
    {
        m_map[place.X, place.Y] = ball;
        m_freePlaces.Remove(place);

        if (m_freePlaces.Count == 0)
        {
            GameOver();
        }
    }
    
    private void FillOutMap(Ball ball)
    {
        m_freePlaces.Add(ball.Place);
        m_map[ball.Place.X, ball.Place.Y] = null;
    }

    private void FillOutMap(SPlace place)
    {
        m_freePlaces.Add(place);
        m_map[place.X, place.Y] = null;
    }

    private bool TryDestroyBall(Ball ball)
    {
        if (!IsFree(ball.Place))
        {
            FillOutMap(ball);
            Destroy(ball.GameObject);
            return true;
        }
        return false;
    }

    private bool CheckPath(SPlace now, SPlace target)
    {
        int[,] pathMap = new int[M_MAP_SIZE, M_MAP_SIZE];
        for (int i = 0; i < M_MAP_SIZE; i++)
        {
            for (int j = 0; j < M_MAP_SIZE; j++)
            {
                pathMap[i, j] = -1;
            }
        }

        pathMap[now.X, now.Y] = 0;

        m_ballPath.Clear();

        if (FindPath(now, target, new List<SPlace>(), new Queue<SPlace>(), pathMap))
        {
            for (int i = 0; i < M_MAP_SIZE; i++)
            {
                string str = "";
                for (int j = 0; j < M_MAP_SIZE; j++)
                {
                    str += pathMap[i, j] + " ";
                }
            }

            DecryptPath(target, pathMap);

            foreach (SPlace place in m_ballPath)
            {
                Debug.Log(place.X + " " + place.Y);
            }

            return true;
        }

        return false;
    }  

    private void DecryptPath(SPlace now, int[,] pathMap)
    {
        if (pathMap[now.X, now.Y] == 0)
        {
            return;
        }

        m_ballPath.Push(now);

        SPlace[] tryPlaces = new SPlace[4];
        tryPlaces[0] = new SPlace(now.X, now.Y + 1);
        tryPlaces[1] = new SPlace(now.X + 1, now.Y);
        tryPlaces[2] = new SPlace(now.X, now.Y - 1);
        tryPlaces[3] = new SPlace(now.X - 1, now.Y);

        for (int i = 0; i < 4; i++)
        {
            if (IsFree(tryPlaces[i]))
            {
                if (pathMap[tryPlaces[i].X, tryPlaces[i].Y] == pathMap[now.X, now.Y] - 1)
                {
                    DecryptPath(tryPlaces[i], pathMap);
                    return;
                }
            }
        }
    }

    private bool FindPath(SPlace now, SPlace target, List<SPlace> passedPlaces, Queue<SPlace> placesToVisit, int[,] pathMap)
    {
        if (now == target)
        {
            return true;
        }

        passedPlaces.Add(now);

        SPlace[] tryPlaces = new SPlace[4];
        tryPlaces[0] = new SPlace(now.X, now.Y + 1);
        tryPlaces[1] = new SPlace(now.X + 1, now.Y);
        tryPlaces[2] = new SPlace(now.X, now.Y - 1);
        tryPlaces[3] = new SPlace(now.X - 1, now.Y);

        for (int i = 0; i < 4; i++)
        {
            if (IsFree(tryPlaces[i]) && !passedPlaces.Contains(tryPlaces[i]))
            {
                placesToVisit.Enqueue(tryPlaces[i]);
                pathMap[tryPlaces[i].X, tryPlaces[i].Y] = pathMap[now.X, now.Y] + 1;
            }
        }

        if (placesToVisit.Count == 0)
        {
            return false;
        }

        SPlace nextPlace = placesToVisit.Dequeue();
        if (FindPath(nextPlace, target, new List<SPlace>(passedPlaces), new Queue<SPlace>(placesToVisit), pathMap))
        {
            return true;
        }

        return false;
    }

    private void PutDownBall()
    {
        SPlace newPlace = m_MousePlace;
        SPlace oldPlace = m_pickedUpBall.Place;

        m_pickedUpBall.MoveTo(m_MousePlace);

        m_ballIsComingSoon = true;

        if ((m_MouseOnTheMap) && (IsFree(newPlace)) && (CheckPath(oldPlace, newPlace)))
        {
            StartCoroutine("PutDownBallByPath");
        }
        else
        {
            StartCoroutine("PutDownBallByLerp");
        }
    }

    private IEnumerator PutDownBallByPath()
    {
        Ball ball = m_map[m_pickedUpBall.Place.X, m_pickedUpBall.Place.Y];

        FillOutMap(ball);
        FillInMap(ball, m_MousePlace);

        ball.Place = m_MousePlace;

        while (m_ballPath.Count > 0)
        {
            ball.MoveTo(m_ballPath.Pop());
            yield return new WaitForSeconds(.1f);
        }

        ball.MoveDown();
        ball.Half = false;

        Destroy(m_pickedUpBall.GameObject);
        m_pickedUpBall = null;

        m_ballIsComingSoon = false;

        DoTurn();
    }

    private IEnumerator PutDownBallByLerp()
    {
        Ball ball = m_map[m_pickedUpBall.Place.X, m_pickedUpBall.Place.Y];
        float lerpSpeed = 0.7f;

        while (Vector3.Distance(m_pickedUpBall.GameObject.transform.position, ball.GameObject.transform.position) > 0.1f) 
        {
            m_pickedUpBall.GameObject.transform.position = Vector3.Lerp(m_pickedUpBall.GameObject.transform.position, ball.GameObject.transform.position, lerpSpeed);
            yield return new WaitForSeconds(.1f);
        }
        
        ball.Half = false;

        Destroy(m_pickedUpBall.GameObject);
        m_pickedUpBall = null;

        m_ballIsComingSoon = false;
    }

    private void Update()
    {
        if (!m_ballIsComingSoon && Input.touchCount > 0)
        {
            m_mousePos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);

            if (m_pickedUpBall != null)
            {
                if(Input.touches[0].phase == TouchPhase.Moved)
                {
                    TryDragBall();
                }

                if (Input.touches[0].phase == TouchPhase.Ended)
                {
                    PutDownBall();
                }
            }
            else
            {
                if (m_MouseOnTheMap)
                {
                    if (Input.touches[0].phase == TouchPhase.Began)
                    {
                        TryPickUpBall();
                    }
                }
            }
        }

        // every 1 sec inc(GameTime)
        m_timer += Time.deltaTime;
        if (m_timer > 1f)
        {
            m_canvasController.GameTime++;
            m_timer = 0; 
        }
    }
    
    private void DoTurn()
    {
        if (!CheckFiveMatch())
        {
            PlaceNextBalls();

            CheckFiveMatch();

            GenerateNextBalls();
        }
    }

    private bool CheckFiveMatch()
    {
        m_destroyLines.Clear();

        for (int i = 0; i < M_MAP_SIZE; i++)
        {
            CheckLine(GetRow(i));
            CheckLine(GetColumn(i));
        }

        for (int i = M_MAP_SIZE; i >= M_MATCH; i--)
        {
            CheckLine(GetPosTopDia(i));
            CheckLine(GetNegTopDia(i));
            CheckLine(GetPosBotDia(i));
            CheckLine(GetNegBotDia(i));
        }

        if (m_destroyLines.Count > 0)
        {
            DestroyLines();
            return true;
        }
        return false;
    }

    private List<Ball> GetRow(int y)
    {
        List<Ball> line = new List<Ball>();
        for (int x = 0; x < M_MAP_SIZE; x++)
        {
            if (m_map[x, y] != null)
            {
                line.Add(m_map[x, y]);
            }
        }
        return line;
    }

    private List<Ball> GetColumn(int x)
    {
        List<Ball> line = new List<Ball>();
        for (int y = 0; y < M_MAP_SIZE; y++)
        {
            if (m_map[x, y] != null)
            {
                line.Add(m_map[x, y]);
            }
        }
        return line;
    }

    private List<Ball> GetPosTopDia(int i)
    {
        List<Ball> line = new List<Ball>();
        for (int j = 0; j < i; j++)
        {
            if (m_map[j, M_MAP_SIZE - i + j] != null)
            {
                line.Add(m_map[j, M_MAP_SIZE - i + j]);
            }
        }
        return line;
    }

    private List<Ball> GetNegBotDia(int i)
    {
        List<Ball> line = new List<Ball>();
        for (int j = 0; j < i; j++)
        {
            if (m_map[j, i - j - 1] != null)
            {
                line.Add(m_map[j, i - j - 1]);
            }
        }
        return line;
    }

    private List<Ball> GetPosBotDia(int i)
    {
        List<Ball> line = new List<Ball>();
        for (int j = 0; j < i; j++)
        {
            if (m_map[M_MAP_SIZE - i + j, j] != null)
            {
                line.Add(m_map[M_MAP_SIZE - i + j, j]);
            }
        }
        return line;
    }

    private List<Ball> GetNegTopDia(int i)
    {
        List<Ball> line = new List<Ball>();
        for (int j = 0; j < i; j++)
        {
            if (m_map[M_MAP_SIZE - i + j, M_MAP_SIZE - 1 - j] != null)
            {
                line.Add(m_map[M_MAP_SIZE - i + j, M_MAP_SIZE - 1 - j]);
            }
        }
        return line;
    }

    private void CheckLine(List<Ball> line)
    {
        List<Ball> matchLine = new List<Ball>();
        int matchCount;

        if (line.Count >= M_MATCH)
        {
            matchLine.Add(line[0]);
            matchCount = 1;
            for (int i = 1; i < line.Count; i++)
            {
                if ((line[i - 1].Type != line[i].Type) 
                    || (Mathf.Abs(line[i - 1].Place.X - line[i].Place.X) > 1)
                    || (Mathf.Abs(line[i - 1].Place.Y - line[i].Place.Y) > 1))
                {
                    if (matchCount >= M_MATCH)
                    {
                        m_destroyLines.AddRange(matchLine);
                    }

                    matchCount = 1;
                    matchLine.Clear();
                }
                else
                {
                    matchCount++;
                }
                matchLine.Add(line[i]);
            }

            if (matchCount >= M_MATCH)
            {
                m_destroyLines.AddRange(matchLine);
            }
        }
    }

    private void DestroyLines()
    {
        foreach(Ball ball in m_destroyLines)
        {
            if (TryDestroyBall(ball))
            {
                m_canvasController.GameScore += m_cost;
            }
        }
    }

    private void GenerateNextBalls()
    {
        foreach (Ball ball in m_nextBalls)
        {
            // find random type of ball
            int r = Random.Range(0, m_ballPrefabs.Length);

            Destroy(ball.GameObject);
            ball.GameObject = Instantiate(m_ballPrefabs[r]);
            ball.Type = r;
            ball.MoveBackToPlace();
        }
    }

    private void PlaceNextBalls()
    {
        foreach (Ball ball in m_nextBalls)
        {
            if (m_freePlaces.Count > 0)
            {
                // find random free place on the map
                SPlace freePlace = m_freePlaces[Random.Range(0, m_freePlaces.Count)];

                Ball newBall = new Ball(ball.Type, ball.GameObject, freePlace);
                FillInMap(newBall, freePlace);
                newBall.MoveBackToPlace();
            }
            else
            {
                GameOver();
            }
        }
    }

    private void GameOver()
    {
        m_canvasController.SignInCanvas.SetActive(true);

        // fill in time and score in SignInCanvas
        m_canvasController.SignInTime = m_canvasController.GameTime;
        m_canvasController.SignInScore = m_canvasController.GameScore;



        gameObject.SetActive(false);
    }

    public void NewGame()
    {
        m_freePlaces.Clear();

        // ini map
        for (int x = 0; x < M_MAP_SIZE; x++)
        {
            for (int y = 0; y < M_MAP_SIZE; y++)
            {
                if (m_map[x, y] != null)
                {
                    Destroy(m_map[x, y].GameObject);
                    m_map[x, y] = null;
                }

                m_freePlaces.Add(new SPlace(x, y));
            }
        }

        // (create + show on the panel) NextBalls
        GenerateNextBalls();

        DoTurn();
        
        m_canvasController.GameTime = 0;
        m_canvasController.GameScore = 0;
        m_timer = 0;
    }
}