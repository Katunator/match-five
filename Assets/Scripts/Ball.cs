﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball
{
    private int m_type;
    private GameObject m_gameObject;
    private SPlace m_place;

    public int Type
    {
        get { return m_type; }
        set { m_type = value; }
    }
    public GameObject GameObject
    {
        get { return m_gameObject; }
        set { m_gameObject = value; }
    }
    public SPlace Place
    {
        get { return m_place; }
        set { m_place = value; }
    }
    public bool Half
    {
        get { return m_gameObject.GetComponent<SpriteRenderer>().color.a == 0.5f; }
        set
        {
            Color c = m_gameObject.GetComponent<SpriteRenderer>().color;

            if (value)
            {
                c.a = 0.5f;

            }
            else
            {
                c.a = 1;
            }

            m_gameObject.GetComponent<SpriteRenderer>().color = c;
        }
    }

    public Ball(int type, GameObject gameObject, SPlace place)
    {
        m_type = type;
        m_place = place;
        m_gameObject = Object.Instantiate(gameObject);

        MoveBackToPlace();
        MoveDown();
    }

    public Ball(Ball ball)
    {
        m_type = ball.Type;
        m_place = ball.Place;
        m_gameObject = Object.Instantiate(ball.GameObject);

        MoveBackToPlace();
        MoveDown();
    }

    public void MoveTo(float x, float y)
    {
        m_gameObject.transform.position = new Vector3(x, y, 0f);
    }

    public void MoveTo(SPlace place)
    {
        m_gameObject.transform.position = new Vector3(place.X, place.Y, 0f);
    }

    public void MoveUp()
    {
        m_gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "PickedUpBall";
    }

    public void MoveDown()
    {
        GameObject.GetComponent<SpriteRenderer>().sortingLayerName = "Default";
    }

    public void MoveBackToPlace()
    {
        m_gameObject.transform.position = new Vector3(m_place.X, m_place.Y, 0f);
    }
}
