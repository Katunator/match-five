﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SPlace
{
    private int m_x;
    private int m_y;

    public int X { get; set; }
    public int Y { get; set; }

    public SPlace(int x, int y)
    {
        m_x = x;
        m_y = y;

        X = m_x;
        Y = m_y;
    }

    public SPlace(GameObject gameObject)
    {
        m_x = Mathf.RoundToInt(gameObject.transform.position.x);
        m_y = Mathf.RoundToInt(gameObject.transform.position.y);

        X = m_x;
        Y = m_y;
    }

    public SPlace(Vector3 vector)
    {
        m_x = Mathf.RoundToInt(vector.x);
        m_y = Mathf.RoundToInt(vector.y);

        X = m_x;
        Y = m_y;
    }

    public SPlace(SPlace place)
    {
        m_x = place.X;
        m_y = place.Y;

        X = m_x;
        Y = m_y;
    }

    public override string ToString()
    {
        return m_x + " " + m_y;
    }

    public static bool operator ==(SPlace p1, SPlace p2)
    {
        return p1.X == p2.X && p1.Y == p2.Y;
    }

    public static bool operator !=(SPlace p1, SPlace p2)
    {
        return p1.X != p2.X || p1.Y != p2.Y;
    }
}
