﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SRecord
{
    public string m_name;
    public int m_time;
    public int m_score;

    public string Name { get; set; }
    public int Time { get; set; }
    public int Score { get; set; }

    public SRecord(string name, int time, int score)
    {
        m_name = name;
        m_time = time;
        m_score = score;

        Name = m_name;
        Time = m_time;
        Score = m_score;
    }
}
